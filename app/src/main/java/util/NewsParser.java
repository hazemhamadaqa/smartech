package util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.List;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.News;


public class NewsParser {

	GsonBuilder gsonBuilder = new GsonBuilder();
	private  Gson gson = gsonBuilder.create();


	/*
	 * High level method that will read the json file and parse it
	 * High level method that will read the json file and parse it
	 * into a List of News objects.
	 */
	public  List<News> getNewsFromFile(Context ctx){
		List<News> news = new ArrayList<News>();
		
		String json = readFile(ctx); // getting json

		News[] aaNews = getGson() .fromJson(json, News[].class);
		for (int i = 0; i < aaNews.length; i++) {
			News n= new News();
			n.setAspnet_Membership(aaNews[i].getAspnet_Membership());
			n.setCreatedBy(aaNews[i].getCreatedBy());
			n.setNewsBody(aaNews[i].getNewsBody());
			n.setNewsBrief(aaNews[i].getNewsBrief());
			n.setNewsDate(aaNews[i].getNewsDate());
			n.setNewsID(aaNews[i].getNewsID());
			n.setNewsTitle(aaNews[i].getNewsTitle());
			n.setNewsImageLargeExt(aaNews[i].getNewsImageLargeExt());
			n.setNewsImageSmallExt(aaNews[i].getNewsImageSmallExt());

			news.add(n);
		}
		//Log.e("number of news is",""+aaNews.length);
		return news;
	}
	
	//Helper method to read the file and return its text.
	private static String readFile(Context ctx){
		String json = "";
		try {
			FileInputStream fis = ctx.openFileInput("News.json");
			BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
			String line;
			while((line = reader.readLine()) != null){
				json += line;
				json += "\n";
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return json;
	}

	public  Gson getGson() {
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}


}
