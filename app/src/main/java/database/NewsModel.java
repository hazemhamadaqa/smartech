package database;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * Created by Eng. Hazem Hamadaqa on 2/18/2016.
 */
public class NewsModel  implements Serializable{

    /**
     * Model class for News database table
     */
    @DatabaseField(columnName = "News_id",canBeNull = false)
    private String NewsID;
    @DatabaseField(columnName = "News_title",canBeNull = false)
    private String NewsTitle;
    @DatabaseField(columnName = "News_brief",canBeNull = false)
    private String NewsBrief;
    @DatabaseField(columnName = "News_body",canBeNull = false)
    private String NewsBody;
    @DatabaseField(columnName = "News_Date",canBeNull = false)
    private String NewsDate;
    @DatabaseField(columnName = "News_imagelargeExt",canBeNull = false)
    private String NewsImageLargeExt;
    @DatabaseField(columnName = "News_imagesmallExt",canBeNull = false)
    private String NewsImageSmallExt;
    @DatabaseField(columnName = "CreatedBy",canBeNull = false)
    private String CreatedBy;
    @DatabaseField(columnName = "aspnet_Membership",canBeNull = false)
    private String aspnet_Membership;

    // Default constructor is needed for the SQLite, so make sure you also have it
    public NewsModel(){

    }
    public NewsModel(String NewsID,String NewsTitle,String NewsBrief,String NewsBody ,String NewsDate,String NewsImageLargeExt,String NewsImageSmallExt,String CreatedBy,String aspnet_Membership){
        this.NewsID=NewsID;
        this.NewsTitle=NewsTitle;
        this.NewsBrief=NewsBrief;
        this.NewsBody=NewsBody;
        this.NewsDate=NewsDate;
        this.NewsImageLargeExt=NewsImageLargeExt;
        this.NewsImageSmallExt=NewsImageSmallExt;
        this.CreatedBy=CreatedBy;
        this.aspnet_Membership=aspnet_Membership;


    }

    public String getAspnet_Membership() {
        return aspnet_Membership;
    }

    public void setAspnet_Membership(String aspnet_Membership) {
        this.aspnet_Membership = aspnet_Membership;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getNewsImageSmallExt() {
        return NewsImageSmallExt;
    }

    public void setNewsImageSmallExt(String newsImageSmallExt) {
        NewsImageSmallExt = newsImageSmallExt;
    }

    public String getNewsImageLargeExt() {
        return NewsImageLargeExt;
    }

    public void setNewsImageLargeExt(String newsImageLargeExt) {
        NewsImageLargeExt = newsImageLargeExt;
    }

    public String getNewsDate() {
        return NewsDate;
    }

    public void setNewsDate(String newsDate) {
        NewsDate = newsDate;
    }

    public String getNewsBody() {
        return NewsBody;
    }

    public void setNewsBody(String newsBody) {
        NewsBody = newsBody;
    }

    public String getNewsBrief() {
        return NewsBrief;
    }

    public void setNewsBrief(String newsBrief) {
        NewsBrief = newsBrief;
    }

    public String getNewsTitle() {
        return NewsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        NewsTitle = newsTitle;
    }

    public String getNewsID() {
        return NewsID;
    }

    public void setNewsID(String newsID) {
        NewsID = newsID;
    }
}
