package fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import adapter.NewsAdapter;
import database.DatabaseHelper;
import database.NewsModel;
import hazemcom.smarttechtaskhazem.R;
import model.News;
import util.Downloader;
import util.NewsParser;


public class MainFragment extends Fragment {
    private ProgressDialog dlg;
    ListView listView;
    NewsAdapter adapter;
    String url="http://212.100.199.141:82/ShuraAPI/api/news/GetAllNews";
    NewsParser s= new NewsParser();
    private DatabaseHelper databaseHelper = null;
    List<News> news = new ArrayList<News>();

    private Dao<NewsModel, Integer> newssDao;
    private List<NewsModel> newsList;

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =inflater.inflate(R.layout.fragment_main, container, false);
        findViews(rootView);

        if(isNetworkAvailable()){
            NewsDownloadTask download = new NewsDownloadTask();
            download.execute();
        }else{
            try {
                newssDao=getHelper().getNewsDao();
                newsList=newssDao.queryForAll();
               // Log.e("size from data base",newsList.size()+"");

                for (int i=0;i<newsList.size();i++){
                    News n= new News();
                    n.setNewsImageSmallExt(newsList.get(i).getNewsImageSmallExt());
                    n.setNewsImageLargeExt(newsList.get(i).getNewsImageLargeExt());
                    n.setNewsTitle(newsList.get(i).getNewsTitle());
                    n.setNewsID(newsList.get(i).getNewsID());
                    n.setAspnet_Membership(newsList.get(i).getAspnet_Membership());
                    n.setNewsDate(newsList.get(i).getNewsDate());
                    n.setCreatedBy(newsList.get(i).getCreatedBy());
                    n.setNewsBody(newsList.get(i).getNewsBody());
                    n.setNewsBrief(newsList.get(i).getNewsBrief());
                    news.add(n);
                }
                adapter = new NewsAdapter(getActivity().getApplicationContext(), -1,news);
                listView.setAdapter(adapter);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return rootView;
    }
    private void findViews( View rootView) {
        listView=(ListView) rootView.findViewById(R.id.listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub

                News n=(News)arg1.getTag();
              //  Toast.makeText(getActivity().getApplicationContext(),""+n.getNewsTitle(),Toast.LENGTH_LONG).show();
                SpecificNew news = SpecificNew.newInstance();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, news).commit();

                Bundle args = new Bundle();

                args.putString("NewsTitle",""+n.getNewsTitle());
                args.putString("NewsImageLargeExt",""+n.getNewsImageLargeExt());
                args.putString("NewsBody",""+n.getNewsBody());
                args.putString("NewsID",""+n.getNewsID());

                news.setArguments(args);

                final Drawable upArrow = ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.back);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(upArrow);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(null);

            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)getActivity(). getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private class NewsDownloadTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dlg = new ProgressDialog(getActivity().getApplicationContext()).show(
                    getActivity(), "Please wait", "data been retrive from ineternet");
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            //Download the file
            try {
                Downloader.DownloadFromUrl(url, getActivity().openFileOutput("News.json", Context.MODE_PRIVATE));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result){

            news=s.getNewsFromFile(getActivity().getApplicationContext());
            adapter = new NewsAdapter(getActivity().getApplicationContext(), -1,news);
            listView.setAdapter(adapter);
            dlg.dismiss();
            if(news.size()>0){
                try {
                    final Dao<NewsModel, Integer> newsDao = getHelper().getNewsDao();
                    for(int i=0;i<news.size();i++){
                        NewsModel n= new NewsModel(news.get(i).getNewsID(),news.get(i).getNewsTitle(),news.get(i).getNewsBrief(),news.get(i).getNewsBody(),news.get(i).getNewsDate(),news.get(i).getNewsImageLargeExt(),news.get(i).getNewsImageSmallExt(),news.get(i).getCreatedBy(),news.get(i).getAspnet_Membership());
                        newsDao.create(n);
                    }
                    Log.i("sucess","sucess");

                } catch (SQLException e) {
                    e.printStackTrace();
                    Log.i("fail", "fail");
                }

            }

            Log.e("Downloaded","Downloaded been");
            }
    }

    // This is how, DatabaseHelper can be initialized for future use
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
		/*
		 * You'll need this in your class to release the helper when done.
		 */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
