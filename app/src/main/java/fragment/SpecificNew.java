package fragment;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import hazemcom.smarttechtaskhazem.R;

public class SpecificNew extends Fragment {
    String url="http://212.100.199.141:82/ShuraAdmin/File/News/Large/";
    TextView title;
    TextView body;
    ImageView image;
    ProgressBar indicator;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    // TODO: Rename and change types and number of parameters
    public static SpecificNew newInstance() {
        SpecificNew fragment = new SpecificNew();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity().getApplicationContext()).build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        //Setup options for ImageLoader so it will handle caching for us.
        options = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .cacheOnDisc()
                .build();

    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        item.setVisible(false);

        MenuItem twitter = menu.findItem(R.id.twitter);
        twitter.setVisible(true);

        MenuItem feedly = menu.findItem(R.id.feedly);
        feedly.setVisible(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_specific_new, container, false);
        findViews(rootView);

        Bundle b = this.getArguments();
        try {
           /// Toast.makeText(getActivity().getApplicationContext(),""+b.getString("NewsTitle",""), Toast.LENGTH_LONG).show();
            title.setText(b.getString("NewsTitle","no title"));
            body.setText( Html.fromHtml(b.getString("NewsBody","no body")));
        }catch (Exception e){
            Toast.makeText(getActivity().getApplicationContext(),""+e.getMessage(),Toast.LENGTH_LONG).show();
        }


        ImageLoadingListener listener = new ImageLoadingListener(){

            @Override
            public void onLoadingStarted(String arg0, View arg1) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onLoadingCancelled(String arg0, View arg1) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
                indicator.setVisibility(View.INVISIBLE);
                image.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
                // TODO Auto-generated method stub
            }
        };

        imageLoader.displayImage(url +b.getString("NewsID") + b.getString("NewsImageLargeExt"), image, options, listener);

        return rootView;

    }

    private void findViews( View rootView) {
         title= (TextView)rootView.findViewById(R.id.title);
         body= (TextView)rootView.findViewById(R.id.body);
         image=(ImageView)rootView.findViewById(R.id.imageView);
         indicator = (ProgressBar)rootView.findViewById(R.id.progress);
        indicator.setVisibility(View.VISIBLE);
        image.setVisibility(View.INVISIBLE);
    }

}
