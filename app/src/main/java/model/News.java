package model;

import com.google.gson.annotations.Expose;

/**
 * Created by Eng. Hazem Hamadaqa on 2/17/2016.
 */
public class News {

    @Expose
    private String NewsID;
    @Expose
    private String NewsTitle;
    @Expose
    private String NewsBrief;
    @Expose
    private String NewsBody;
    @Expose
    private String NewsDate;
    @Expose
    private String NewsImageLargeExt;
    @Expose
    private String NewsImageSmallExt;
    @Expose
    private String CreatedBy;
    @Expose
    private String aspnet_Membership;




    public String getNewsBody() {
        return NewsBody;
    }

    public void setNewsBody(String newsBody) {
        NewsBody = newsBody;
    }

    public String getNewsBrief() {
        return NewsBrief;
    }

    public void setNewsBrief(String newsBrief) {
        NewsBrief = newsBrief;
    }

    public String getNewsID() {
        return NewsID;
    }

    public void setNewsID(String newsID) {
        NewsID = newsID;
    }

    public String getNewsImageLargeExt() {
        return NewsImageLargeExt;
       // return "http://212.100.199.141:82/ShuraAdmin/File/News/Large/"+getNewsID()+ NewsImageLargeExt;
    }

    public void setNewsImageLargeExt(String newsImageLargeExt) {
        NewsImageLargeExt = newsImageLargeExt;
    }

    public String getNewsTitle() {
        return NewsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        NewsTitle = newsTitle;
    }

    @Override
    public String toString() {
        return "News [NewsBody=" + NewsBody + ", NewsBrief=" + NewsBrief + ", NewsID="
                + NewsID + ", NewsImageLargeExt=" + NewsImageLargeExt+ ", NewsTitle=" + NewsTitle + "]";
    }

    public String getNewsImageSmallExt() {
        return NewsImageSmallExt;
    }

    public void setNewsImageSmallExt(String newsImageSmallExt) {
        NewsImageSmallExt = newsImageSmallExt;
    }

    public String getAspnet_Membership() {
        return aspnet_Membership;
    }

    public void setAspnet_Membership(String aspnet_Membership) {
        this.aspnet_Membership = aspnet_Membership;
    }

    public String getNewsDate() {
        return NewsDate;
    }

    public void setNewsDate(String newsDate) {
        NewsDate = newsDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }
}
