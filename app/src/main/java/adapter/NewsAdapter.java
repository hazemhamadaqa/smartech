package adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import hazemcom.smarttechtaskhazem.R;
import model.News;

/**
 * Created by Eng. Hazem Hamadaqa on 2/17/2016.
 */
public class NewsAdapter extends ArrayAdapter<News> {
    private LayoutInflater inflater=null;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    String url="http://212.100.199.141:82/ShuraAdmin/File/News/Large/";

    public NewsAdapter(Context ctx, int textViewResourceId, List<News> sites) {
        super(ctx, textViewResourceId, sites);

        inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(ctx).build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        //Setup options for ImageLoader so it will handle caching for us.
        options = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .cacheOnDisc()
                .build();
    }

    @Override
    public View getView(int arg0, View arg1, ViewGroup arg2) {

        View vi=arg1;
        if(arg1==null)
              vi = inflater.inflate(R.layout.list_row, null);

        final ImageView newsPhoto = (ImageView)vi.findViewById(R.id.newsPhoto);
        TextView newstitle = (TextView)vi.findViewById(R.id.newstitle);
        TextView newsdescreption = (TextView)vi.findViewById(R.id.newsdescreption);
        final ProgressBar indicator = (ProgressBar)vi.findViewById(R.id.progress);

        indicator.setVisibility(View.VISIBLE);
        newsPhoto.setVisibility(View.INVISIBLE);

        ImageLoadingListener listener = new ImageLoadingListener(){

            @Override
            public void onLoadingStarted(String arg0, View arg1) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onLoadingCancelled(String arg0, View arg1) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
                indicator.setVisibility(View.INVISIBLE);
                newsPhoto.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
                // TODO Auto-generated method stub
            }
        };

        imageLoader.displayImage(url+getItem(arg0).getNewsID()+getItem(arg0).getNewsImageLargeExt(), newsPhoto,options, listener);

        //Set the relavent text in our TextViews
        newstitle.setText(getItem(arg0).getNewsTitle());
        newsdescreption.setText(getItem(arg0).getNewsBrief());

        vi.setTag(getItem(arg0));
        return vi;
    }
}
